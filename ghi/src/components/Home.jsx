import { useState } from 'react'
import { Link } from 'react-router-dom'
import { useAuthenticateQuery } from '../app/apiSlice'
import banner_3 from '/src/banner_3.jpg'

function Home() {
    const { data: user, isLoading: isLoadingUser } = useAuthenticateQuery()
    const [activeIndex, setActiveIndex] = useState(0)

    const handlePrev = () => {
        setActiveIndex(activeIndex === 0 ? reviews.length - 1 : activeIndex - 1)
    }

    const handleNext = () => {
        setActiveIndex(activeIndex === reviews.length - 1 ? 0 : activeIndex + 1)
    }

    const reviews = [
        {
            name: 'Stephany M.',
            position: 'Senior Solutions Engineer',
            image: '/src/bb.png',
            testimonial:
                'Joining the Domuso team allowed me to continue to drive innovation in the industry I owe my career too. As an SE I get to problem solve for the 50 operators in the industry! Grateful to be apart of this amazing team! ',
        },
        {
            name: 'Tony C.',
            position: 'Sales Leadership',
            image: '/src/tony.png',
            testimonial:
                'Grateful for an amazing team. I get the honor of working with the most professional sales team in the industry!',
        },

        {
            name: 'Jennifer W.',
            position: 'Chief Customer Officer',
            image: '/src/jennifer.png',
            testimonial:
                'Leadership at Domuso encourages me to continue to push myself and innovate. The culture is truly unmatched!',
        },
        {
            name: 'Ron K.',
            position: 'Head of Product',
            image: '/src/ron.png',
            testimonial:
                'Since joining Domuso I have felt such a dedication to my career growth!',
        },
    ]

    if (isLoadingUser) {
        return <div>Loading...</div>
    }

    return (
        <div>
            <div
                style={{
                    margin: '0 auto',
                    maxWidth: '100%',
                    position: 'relative',
                }}
            >
                <img
                    className="d-block user-select-none"
                    src={banner_3}
                    style={{ width: '100%', height: '450px' }}
                    alt="Banner"
                />
                <div
                    style={{
                        position: 'absolute',
                        top: '65%',
                        left: '28%',
                        transform: 'translate(-50%, -50%)',
                    }}
                >
                    {user ? (
                        <Link
                            to="/jobs"
                            className="btn"
                            style={{
                                backgroundColor: '#F26925',
                                color: 'white',
                                border: 'none',
                                padding: '10px 20px',
                                fontSize: '20px',
                            }}
                        >
                            View Jobs
                        </Link>
                    ) : (
                        <Link
                            to="/signup"
                            className="btn"
                            style={{
                                backgroundColor: '#F26925',
                                color: 'white',
                                border: 'none',
                                padding: '10px 20px',
                                fontSize: '20px',
                            }}
                        >
                            Apply Today
                        </Link>
                    )}
                </div>
            </div>
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-5">
                        <div className="grid-container">
                            <div
                                className="grid-box"
                                style={{
                                    marginTop: '130px',
                                    marginBottom: '30px',
                                    display: 'flex',
                                    flexDirection: 'column',
                                    alignItems: 'center',
                                }}
                            >
                                <h1
                                    style={{
                                        marginBottom: '30px',
                                        marginTop: '-30px',
                                        fontSize: '67px',
                                        color: '#F26925',
                                        textAlign: 'left',
                                    }}
                                >
                                    Life at Domuso
                                </h1>
                                <div
                                    className="vl"
                                    style={{
                                        paddingLeft: '3px',
                                        borderLeft: '3px solid #5a414b',
                                        height: '130px',
                                        marginBottom: '30px',
                                    }}
                                >
                                    <p
                                        className="mb-9"
                                        style={{
                                            marginLeft: '10px',
                                            display: 'inline-block',
                                            width: '400px',
                                            color: 'black',
                                            textAlign: 'left',
                                        }}
                                    >
                                        At Domuso, our people power our success:
                                        We believe where employees thrive,
                                        customers thrive too. Join our talent
                                        network today to find your next
                                        position.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-5">
                        <div className="grid-container">
                            <div
                                className="grid-box"
                                style={{
                                    marginBottom: '140px',
                                    marginTop: '150px',
                                }}
                            >
                                <img
                                    className="d-block user-select-none img-fluid"
                                    src="https://images.unsplash.com/photo-1580894908361-967195033215?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8OXx8d29tZW4lMjBpbiUyMHRlY2h8ZW58MHx8MHx8fDI%3D"
                                    alt="Sample"
                                    style={{
                                        borderRadius: '8px',
                                        marginLeft: '30px',
                                        marginTop: '-30px',
                                    }}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <h1
                className="card-title"
                style={{
                    backgroundColor: 'transparent',
                    color: '#0B0F1E',
                    border: 'none',
                    marginTop: '-60px',
                    marginBottom: '30px',
                    textAlign: 'center',
                    fontSize: '60px',
                }}
            >
                Our Values
            </h1>
            <div className="container d-flex justify-content-center">
                <div className="row justify-content-center">
                    <div className="col-md-2 m-1">
                        <div className="d-flex justify-content-center">
                            <div
                                className="shadow-sm p-3 mb-5 bg-body-tertiary rounded"
                                style={{
                                    height: '400px',
                                    width: 'auto',
                                    textAlign: 'center',
                                }}
                            >
                                <img
                                    src="/src/home_2.png"
                                    alt="team respecting each other"
                                    style={{
                                        height: '120px',
                                        width: '180px',
                                        objectFit: 'contain',
                                        display: 'block',
                                        margin: 'auto',
                                    }}
                                />
                                <p>{''}</p>
                                <h5>Respect</h5>
                                <p>{''}</p>
                                <p>
                                    Encouraging an atmosphere of respect where
                                    diverse perspectives are valued and
                                    celebrated.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-2 m-1">
                        <div className="d-flex justify-content-center">
                            <div
                                className="shadow-sm p-3 mb-5 bg-body-tertiary rounded"
                                style={{
                                    height: '400px',
                                    width: '100%',
                                    textAlign: 'center',
                                }}
                            >
                                <img
                                    src="/src/home_1.png"
                                    alt="Team working together"
                                    style={{
                                        height: '120px',
                                        width: '180px',
                                        objectFit: 'contain',
                                        display: 'block',
                                        margin: 'auto',
                                    }}
                                />
                                <p>{''}</p>
                                <h5>Accountability</h5>
                                <p>{''}</p>
                                <p>
                                    Taking ownership and delivering on
                                    commitments with unwavering dedication.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-2 m-1">
                        <div className="d-flex justify-content-center">
                            <div
                                className="shadow-sm p-3 mb-5 bg-body-tertiary rounded"
                                style={{
                                    height: '400px',
                                    width: '100%',
                                    textAlign: 'center',
                                }}
                            >
                                <img
                                    src="/src/home_3.png"
                                    alt="Woman pushing blocks"
                                    style={{
                                        height: '120px',
                                        width: '120px',
                                        objectFit: 'contain',
                                        display: 'block',
                                        margin: 'auto',
                                    }}
                                />
                                <p>{''}</p>
                                <h5>Drive</h5>
                                <p>{''}</p>
                                <p>
                                    Fostering a relentless pursuit of growth and
                                    continuous improvement.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-2 m-1">
                        <div className="d-flex justify-content-center">
                            <div
                                className="shadow-sm p-3 mb-5 bg-body-tertiary rounded"
                                style={{
                                    height: '400px',
                                    width: '100%',
                                    textAlign: 'center',
                                }}
                            >
                                <img
                                    src="/src/home_4.png"
                                    alt="Brain in a box"
                                    style={{
                                        height: '120px',
                                        width: '120px',
                                        objectFit: 'contain',
                                        display: 'block',
                                        margin: 'auto',
                                    }}
                                />
                                <p>{''}</p>
                                <h5>Innovation</h5>
                                <p>{''}</p>
                                <p>
                                    Empowering creative thinking to redefine
                                    possibilities and drive meaningful change.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-6">
                        <div className="grid-container">
                            <div
                                className="grid-box"
                                style={{
                                    marginTop: '100px',
                                    marginBottom: '40px',
                                }}
                            >
                                <img
                                    className="d-block user-select-none img-fluid"
                                    src="https://images.unsplash.com/photo-1573165265437-f5e267bb3db6?q=80&w=2069&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
                                    alt="Sample"
                                    style={{
                                        borderRadius: '8px',
                                        height: '100%',
                                        width: '90%',
                                    }}
                                />
                            </div>
                        </div>
                    </div>
                    <div className="col-md-5">
                        <div className="grid-container">
                            <div
                                className="grid-box"
                                style={{
                                    marginTop: '150px',
                                    marginBottom: '40px',
                                }}
                            >
                                <div className="d-flex align-items-center">
                                    <div
                                        className="vl"
                                        style={{
                                            paddingLeft: '3px',
                                            borderLeft: '3px solid #5a414b',
                                            height: '60px',
                                        }}
                                    >
                                        <h1
                                            className="mb-2"
                                            style={{
                                                marginLeft: '10px',
                                                color: '#F26925',
                                            }}
                                        >
                                            Feel the Domuso Difference.
                                        </h1>
                                    </div>
                                </div>
                                <div>
                                    <p style={{ marginTop: '10px' }}>
                                        Are you a tech enthusiast eager to
                                        advance your career? At Domuso, our
                                        people power our success: We believe
                                        where employees thrive, customers thrive
                                        too.
                                    </p>
                                    <div className="d-flex justify-content-start mt-3">
                                        <div className="col">
                                            {user ? (
                                                <Link
                                                    to="/profile"
                                                    type="button"
                                                    className="btn btn-block"
                                                    style={{
                                                        backgroundColor:
                                                            '#F26925',
                                                        color: 'white',
                                                        border: 'none',
                                                        textAlign: 'center',
                                                        width: '160px',
                                                        marginLeft: '0px',
                                                        marginTop: '20px',
                                                    }}
                                                >
                                                    Profile
                                                </Link>
                                            ) : (
                                                <Link
                                                    to="/signup"
                                                    type="button"
                                                    className="btn btn-block"
                                                    style={{
                                                        backgroundColor:
                                                            '#F26925',
                                                        color: 'white',
                                                        border: 'none',
                                                        textAlign: 'center',
                                                        width: '160px',
                                                        marginLeft: '0px',
                                                        marginTop: '20px',
                                                    }}
                                                >
                                                    Apply Today
                                                </Link>
                                            )}
                                        </div>
                                        <div className="col">
                                            {user ? (
                                                <Link
                                                    to="/applications"
                                                    type="button"
                                                    className="btn btn-block"
                                                    style={{
                                                        backgroundColor:
                                                            '#F26925',
                                                        color: 'white',
                                                        border: 'none',
                                                        textAlign: 'center',
                                                        width: '160px',
                                                        marginLeft: '-30px',
                                                        marginRight: '60px',
                                                        marginTop: '20px',
                                                    }}
                                                >
                                                    My Applications
                                                </Link>
                                            ) : (
                                                <Link
                                                    to="/signin"
                                                    type="button"
                                                    className="btn btn-block"
                                                    style={{
                                                        backgroundColor:
                                                            '#F26925',
                                                        color: 'white',
                                                        border: 'none',
                                                        textAlign: 'center',
                                                        width: '160px',
                                                        marginRight: '60px',
                                                        marginTop: '20px',
                                                    }}
                                                >
                                                    View Applications
                                                </Link>
                                            )}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row justify-content-center mt-5">
                    <div className="col-md-5">
                        <div className="grid-container">
                            <div
                                className="grid-box"
                                style={{
                                    marginTop: '50px',
                                    marginBottom: '40px',
                                }}
                            >
                                <div className="d-flex align-items-center">
                                    <div
                                        className="vl"
                                        style={{
                                            paddingLeft: '3px',
                                            borderLeft: '3px solid #5a414b',
                                            height: '55px',
                                        }}
                                    >
                                        <h1
                                            className="mb-2"
                                            style={{
                                                marginLeft: '10px',
                                                color: '#5a414b',
                                            }}
                                        >
                                            Domuso HR Team
                                        </h1>
                                    </div>
                                </div>
                                <p style={{ marginTop: '10px' }}>
                                    Our philosophy is simple – hire a team of
                                    diverse, passionate people and foster a
                                    culture that empowers you to do you best
                                    work.
                                </p>
                                <div className="d-flex justify-content-start mt-3">
                                    {user ? (
                                        <div className="col">
                                            <Link
                                                to="/createjob"
                                                type="button"
                                                className="btn btn-block"
                                                style={{
                                                    backgroundColor: '#0B0F1E',
                                                    color: 'white',
                                                    border: 'none',
                                                    textAlign: 'center',
                                                    width: '160px',
                                                    marginLeft: '0px',
                                                    marginTop: '20px',
                                                }}
                                            >
                                                Post a Job
                                            </Link>
                                        </div>
                                    ) : (
                                        <>
                                            <div className="col">
                                                <Link
                                                    to="/signin"
                                                    type="button"
                                                    className="btn btn-block"
                                                    style={{
                                                        backgroundColor:
                                                            '#0B0F1E',
                                                        color: 'white',
                                                        border: 'none',
                                                        textAlign: 'center',
                                                        width: '160px',
                                                        marginLeft: '0px',
                                                        marginTop: '20px',
                                                    }}
                                                >
                                                    Sign In
                                                </Link>
                                            </div>
                                            <div className="col">
                                                <Link
                                                    to="/signup"
                                                    type="button"
                                                    className="btn btn-block"
                                                    style={{
                                                        backgroundColor:
                                                            '#0B0F1E',
                                                        color: 'white',
                                                        border: 'none',
                                                        textAlign: 'center',
                                                        width: '160px',
                                                        marginRight: '30px',
                                                        marginTop: '20px',
                                                    }}
                                                >
                                                    Create Account
                                                </Link>
                                            </div>
                                        </>
                                    )}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="grid-container">
                            <div
                                className="grid-box"
                                style={{
                                    marginBottom: '150px',
                                }}
                            >
                                <img
                                    className="d-block user-select-none img-fluid"
                                    src="https://images.unsplash.com/photo-1573167243872-43c6433b9d40?q=80&w=2669&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
                                    alt="Sample"
                                    style={{
                                        borderRadius: '8px',
                                        height: '100%',
                                        width: '90%',
                                        marginLeft: '50px',
                                    }}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div
                id="carouselExampleControls"
                className="carousel slide text-center carousel-dark"
                data-bs-ride="carousel"
            >
                <div className="carousel-inner">
                    {reviews.map((review, index) => (
                        <div
                            className={`carousel-item ${
                                index === activeIndex ? 'active' : ''
                            }`}
                            key={index}
                            style={{ minHeight: '400px' }}
                        >
                            <img
                                className="rounded-circle shadow-1-strong mb-4"
                                src={review.image}
                                alt={review.name}
                                style={{ width: '150px' }}
                            />
                            <div className="row d-flex justify-content-center">
                                <div className="col-lg-8">
                                    <h3 className="mb-3">{review.name}</h3>
                                    <p className="text-muted">{review.position}</p>
                                    <h className="text-bold">
                                        <i className="fas fa-quote-left pe-2"></i>
                                        {review.testimonial}
                                    </h>
                                </div>
                            </div>
                        </div>
                    ))}
                </div>
                <button
                    className="carousel-control-prev"
                    type="button"
                    onClick={handlePrev}
                >
                    <span
                        className="carousel-control-prev-icon"
                        aria-hidden="true"
                    ></span>
                    <span className="visually-hidden">Previous</span>
                </button>
                <button
                    className="carousel-control-next"
                    type="button"
                    onClick={handleNext}
                >
                    <span
                        className="carousel-control-next-icon"
                        aria-hidden="true"
                    ></span>
                    <span className="visually-hidden">Next</span>
                </button>
            </div>
        </div>
    )
}

export default Home
