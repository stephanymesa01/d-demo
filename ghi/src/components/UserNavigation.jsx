import { Link, useLocation } from 'react-router-dom'

const UserNavigation = () => {
    const location = useLocation()

    return (
        <nav
            className="col-md-3 col-lg-2 d-md-block bg-light sidebar"
            style={{ marginTop: '20px' }}
        >
            <div className="position-sticky">
                <div className="list-group">
                    <Link
                        to="/profile"
                        style={
                            location.pathname === '/profile'
                                ? { backgroundColor: '#F26925', color: 'white' }
                                : {}
                        }
                        className="list-group-item list-group-item-action"
                    >
                        Profile
                    </Link>
                    <Link
                        to="/applications"
                        style={
                            location.pathname === '/applications'
                                ? { backgroundColor: '#F26925', color: 'white' }
                                : {}
                        }
                        className="list-group-item list-group-item-action"
                    >
                        My Applications
                    </Link>
                    <Link
                        to="/mypostedjobs"
                        style={
                            location.pathname === '/mypostedjobs'
                                ? { backgroundColor: '#F26925', color: 'white' }
                                : {}
                        }
                        className="list-group-item list-group-item-action"
                    >
                        Posted Jobs
                    </Link>
                    <Link
                        to="/createjob"
                        style={
                            location.pathname === '/createjob'
                                ? { backgroundColor: '#F26925', color: 'white' }
                                : {}
                        }
                        className="list-group-item list-group-item-action"
                    >
                        Post a Job
                    </Link>
                </div>
            </div>
        </nav>
    )
}

export default UserNavigation
