import { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import {
    useAuthenticateQuery,
    useLazyListAllJobsByPosterQuery,
    useDeleteJobMutation,
} from '../app/apiSlice'
import UserNavigation from './UserNavigation'

const MyPostedJobs = () => {
    const navigate = useNavigate()
    const [deleteID, setDeleteID] = useState('')
    const [myJobs, setMyJobs] = useState([])
    const { data: user, isLoading: isLoadingUser } = useAuthenticateQuery()
    const [listJobsTrigger, listJobsResult] = useLazyListAllJobsByPosterQuery()
    const [deleteJob] = useDeleteJobMutation()
    const [currentPage, setCurrentPage] = useState(1)

    const jobsPerPage = 8

    useEffect(() => {
        if (!user && !isLoadingUser) {
            navigate('/signin')
        } else if (user) {
            listJobsTrigger()
        }
    }, [user, isLoadingUser, navigate, listJobsTrigger])

    useEffect(() => {
        if (listJobsResult.isSuccess) {
            setMyJobs(listJobsResult.data.jobs)
        } else if (listJobsResult.isError) {
            setMyJobs([])
        }
    }, [listJobsResult])

    if (listJobsResult.isLoading) {
        return <div>Loading Jobs You've Posted...</div>
    }

    const indexOfLastJob = currentPage * jobsPerPage
    const indexOfFirstJob = indexOfLastJob - jobsPerPage
    const currentJobs = myJobs.slice(indexOfFirstJob, indexOfLastJob)

    const formatDate = (dateString) => {
        const date = new Date(dateString)
        const month = date.toLocaleString('default', { month: 'short' })
        const day = date.getDate()
        const year = date.getFullYear()
        return `${month}-${day}-${year}`
    }

    const paginate = (pageNumber) => setCurrentPage(pageNumber)

    const handleDelete = (jobID) => {
        setDeleteID(jobID)
        deleteJob(jobID)
    }

    return (
        <div className="container-fluid" style={{ minHeight: '80vh' }}>
            <div className="row">
                <UserNavigation />
                <main
                    className="col-md-9 ms-sm-auto col-lg-10 px-md-4"
                    style={{ marginTop: '20px' }}
                >
                    <div>
                        <h2>Posted Jobs</h2>
                        <table className="table table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">Department</th>
                                    <th scope="col">Position</th>
                                    <th scope="col">Posted Date</th>
                                    <th scope="col">Applicants</th>
                                    <th scope="col">Remind Me</th>
                                    <th scope="col">Remove</th>
                                </tr>
                            </thead>
                            <tbody>
                                {currentJobs.length === 0 ? (
                                    <tr>
                                        <td colSpan="6">
                                            No job postings at this time
                                        </td>
                                    </tr>
                                ) : (
                                    currentJobs.map((job, index) => (
                                        <tr
                                            key={index}
                                            className={
                                                index % 2 === 0
                                                    ? 'table-light'
                                                    : 'table-light'
                                            }
                                        >
                                            <td>{job.company_name}</td>
                                            <td>{job.position_title}</td>
                                            <td>
                                                {formatDate(job.posted_date)}
                                            </td>
                                            <td>
                                                <button
                                                    type="button"
                                                    className="btn btn-outline-secondary"
                                                    style={{
                                                        color: '#332B3B',
                                                        borderColor: '#332B3B',
                                                    }}
                                                    onClick={() =>
                                                        navigate(
                                                            `/jobs/${job.id}/applications`
                                                        )
                                                    }
                                                >
                                                    See Applicants
                                                </button>
                                            </td>
                                            <td>
                                                <div
                                                    style={{
                                                        display: 'flex',
                                                        justifyContent: 'left',
                                                        color: 'black',
                                                    }}
                                                >
                                                    <button
                                                        type="button"
                                                        className="btn btn-outline-secondary"
                                                        style={{
                                                            color: 'black',
                                                            borderColor:
                                                                'black',
                                                        }}
                                                        onClick={() =>
                                                            navigate(
                                                                `/jobs/${job.id}`
                                                            )
                                                        }
                                                    >
                                                        Job Detail
                                                    </button>
                                                </div>
                                            </td>
                                            <td>
                                                <button
                                                    type="button"
                                                    className="btn btn-outline-danger"
                                                    onClick={() =>
                                                        handleDelete(job.id)
                                                    }
                                                >
                                                    Delete Job
                                                </button>
                                            </td>
                                        </tr>
                                    ))
                                )}
                            </tbody>
                        </table>
                    </div>
                    <button
                        type="button"
                        className="btn d-flex align-items-right"
                        style={{
                            color: '#F26925',
                            borderColor: '#F26925',
                            marginTop: '15px',
                            backgroundColor: 'transparent',
                        }}
                        onClick={() => navigate('/createjob')}
                    >
                        Post a Job
                    </button>

                    <div className="d-flex justify-content-end">
                        <ul
                            className="pagination pagination-sm"
                            style={{ color: '#300b9b' }}
                        >
                            <li
                                className={`page-item ${
                                    currentPage === 1 ? 'disabled' : ''
                                }`}
                            >
                                <a
                                    className="page-link"
                                    href="#"
                                    onClick={() => paginate(currentPage - 1)}
                                >
                                    &laquo;
                                </a>
                            </li>
                            {Array.from(
                                {
                                    length: Math.ceil(
                                        myJobs.length / jobsPerPage
                                    ),
                                },
                                (_, i) => (
                                    <li
                                        key={i}
                                        className={`page-item ${
                                            currentPage === i + 1
                                                ? 'active'
                                                : ''
                                        }`}
                                    >
                                        <a
                                            className="page-link"
                                            href="#"
                                            onClick={() => paginate(i + 1)}
                                        >
                                            {i + 1}
                                        </a>
                                    </li>
                                )
                            )}
                            <li
                                className={`page-item ${
                                    currentPage ===
                                    Math.ceil(myJobs.length / jobsPerPage)
                                        ? 'disabled'
                                        : ''
                                }`}
                            >
                                <a
                                    className="page-link"
                                    href="#"
                                    onClick={() => paginate(currentPage + 1)}
                                >
                                    &raquo;
                                </a>
                            </li>
                        </ul>
                    </div>
                </main>
            </div>
        </div>
    )
}

export default MyPostedJobs
