export default function HeroImage() {
  return (
    <header style={{ paddingLeft: 0 }}>
      <div
        className='p-5 text-center bg-image'
        style={{
          backgroundImage: "url('/src/team.jpg')",
          height: 600,
          backgroundSize: 'cover',
          backgroundPosition: 'center',
        }}
      >
        <div className='mask' style={{ backgroundColor: 'rgba(242, 105, 37, 0.6)' }}>
          <div className='d-flex justify-content-center align-items-center h-100'>
            <div className='text-white'>
              <h1 className='mb-3'>Join Domuso</h1>
              <h4 className='mb-3'>Feel the Domuso Difference</h4>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
};
