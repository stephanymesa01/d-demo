import { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import UserNavigation from './UserNavigation'
import JobDetailForApp from './JobDetailForApp'
import {
    useAuthenticateQuery,
    useLazyListAllAppsForJobseekerQuery,
    useDeleteAppMutation,
} from '../app/apiSlice'

const AppliedJobs = () => {
    const navigate = useNavigate()
    const [appliedJobs, setAppliedJobs] = useState([])
    const [currentPage, setCurrentPage] = useState(1)
    const { data: user, isLoading: isLoadingUser } = useAuthenticateQuery()
    const [appListTrigger, appListResult] =
        useLazyListAllAppsForJobseekerQuery()
    const [deleteApp] = useDeleteAppMutation()

    const jobsPerPage = 8

    useEffect(() => {
        if (!user && !isLoadingUser) {
            navigate('/signin')
        } else if (user) {
            appListTrigger()
        }
    }, [user, isLoadingUser, navigate, appListTrigger])

    useEffect(() => {
        if (appListResult.isSuccess) {
            setAppliedJobs(appListResult.data.applications)
        } else if (appListResult.isError) {
            setAppliedJobs([])
        }
    }, [appListResult])

    if (appListResult.isLoading) {
        return <div>Loading your applications...</div>
    }

    const formatDate = (dateString) => {
        const date = new Date(dateString)
        const month = date.toLocaleString('default', { month: 'short' })
        const day = date.getDate()
        const year = date.getFullYear()
        return `${month}-${day}-${year}`
    }

    const indexOfLastJob = currentPage * jobsPerPage
    const indexOfFirstJob = indexOfLastJob - jobsPerPage
    const currentJobs = appliedJobs.slice(indexOfFirstJob, indexOfLastJob)

    const paginate = (pageNumber) => setCurrentPage(pageNumber)

    const handleDelete = (appID) => {
        deleteApp(appID)
    }

    return (
        <div className="container-fluid" style={{ minHeight: '80vh' }}>
            <div className="row">
                <UserNavigation />
                <main
                    className="col-md-10 ms-sm-auto col-lg-15 px-md-6"
                    style={{ marginTop: '20px' }}
                >
                    <div>
                        <h2>My Applications</h2>
                        <table className="table table-hover">
                            <thead>
                                <tr>
                                    {/* <th scope="col">Company</th> */}
                                    <th scope="col">Position</th>
                                    <th scope="col">Applied Date</th>
                                    <th scope="col">Remind Me</th>
                                    <th scope="col">Remove me</th>
                                </tr>
                            </thead>
                            <tbody>
                                {currentJobs.length === 0 ? (
                                    <tr>
                                        <td colSpan="5">
                                            No applications at this time
                                        </td>
                                    </tr>
                                ) : (
                                    currentJobs.map((app, index) => (
                                        <tr
                                            key={index}
                                            className={
                                                index % 2 === 0
                                                    ? 'table-light'
                                                    : 'table-light'
                                            }
                                        >
                                            <JobDetailForApp
                                                jobID={app.job_id}
                                            />
                                            <td>
                                                {formatDate(app.applied_at)}
                                            </td>
                                            <td>
                                                <button
                                                    type="button"
                                                    className="btn btn-secondary"
                                                    onClick={() =>
                                                        navigate(
                                                            `/jobs/${app.job_id}`
                                                        )
                                                    }
                                                >
                                                    Job Detail
                                                </button>
                                            </td>
                                            <td>
                                                <button
                                                    type="button"
                                                    className="btn btn-outline-danger"
                                                    onClick={() =>
                                                        handleDelete(app.id)
                                                    }
                                                >
                                                    Delete Application
                                                </button>
                                            </td>
                                        </tr>
                                    ))
                                )}
                            </tbody>
                        </table>
                    </div>
                    <button
                        type="button"
                        className="btn d-flex align-items-right justify-content-end"
                        style={{
                            color: '#F26925',
                            borderColor: '#F26925',
                            marginTop: '15px',
                            backgroundColor: 'transparent',
                        }}
                        onClick={() => navigate('/jobs')}
                    >
                        View Jobs
                    </button>

                    <div className="d-flex justify-content-end">
                        <ul
                            className="pagination pagination-sm"
                            style={{ color: '#300b9b' }}
                        >
                            <li
                                className={`page-item ${
                                    currentPage === 1 ? 'disabled' : ''
                                }`}
                            >
                                <a
                                    className="page-link"
                                    href="#"
                                    onClick={() => paginate(currentPage - 1)}
                                >
                                    &laquo;
                                </a>
                            </li>
                            {Array.from(
                                {
                                    length: Math.ceil(
                                        appliedJobs.length / jobsPerPage
                                    ),
                                },
                                (_, i) => (
                                    <li
                                        key={i}
                                        className={`page-item ${
                                            currentPage === i + 1
                                                ? 'active'
                                                : ''
                                        }`}
                                    >
                                        <a
                                            className="page-link"
                                            href="#"
                                            onClick={() => paginate(i + 1)}
                                        >
                                            {i + 1}
                                        </a>
                                    </li>
                                )
                            )}
                            <li
                                className={`page-item ${
                                    currentPage ===
                                    Math.ceil(appliedJobs.length / jobsPerPage)
                                        ? 'disabled'
                                        : ''
                                }`}
                            >
                                <a
                                    className="page-link"
                                    href="#"
                                    onClick={() => paginate(currentPage + 1)}
                                >
                                    &raquo;
                                </a>
                            </li>
                        </ul>
                    </div>
                </main>
            </div>
        </div>
    )
}

export default AppliedJobs
