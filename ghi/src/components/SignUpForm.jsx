import { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import { useSignupMutation } from '../app/apiSlice'
import workingwoman from '/src/workingwoman.mp4'
import HeroImage from './HeroImage'

export default function SignUpForm() {
    const navigate = useNavigate()
    const [formData, setFormData] = useState({
        username: '',
        password: '',
        full_name: '',
        email: '',
        linkedin_url: '',
    })
    const [errorMessage, setErrorMessage] = useState('')
    const [showModal, setShowModal] = useState(false)
    const [signup, signupStatus] = useSignupMutation()

    useEffect(() => {
        if (signupStatus.isSuccess) {
            navigate('/')
        } else if (signupStatus.isError) {
            setErrorMessage(signupStatus.error.data.detail)
            setShowModal(true)
        }
    }, [signupStatus, navigate, setErrorMessage, setShowModal])

    const handleFormSubmit = (e) => {
        e.preventDefault()
        if (
            !formData.username ||
            !formData.password ||
            !formData.email ||
            !formData.linkedin_url ||
            !formData.full_name
        ) {
            setErrorMessage('Please fill out all required fields.')
            setShowModal(true)
        } else {
            signup(formData)
        }
    }

    const closeModal = () => {
        setShowModal(false)
        setErrorMessage('')
    }

    const [openAccordionItem, setOpenAccordionItem] = useState(null)

    const toggleAccordionItem = (index) => {
        setOpenAccordionItem(openAccordionItem === index ? null : index)
    }

    const accordionItems = [
        {
            title: 'Why do we believe in a remote first culture?',
            body: 'Remote work policy enables a geographically diverse workforce with employees located across the country.',
        },
        {
            title: 'Why do we offer Flexible PTO?',
            body: 'Flexible PTO empowers employees to take the time they need for personal and professional well-being.',
        },
        {
            title: 'What is the culture like at Domuso?',
            body: 'Open communication, diversity, and innovation are encouraged, fostering a supportive and inclusive culture..',
        },
    ]

    return (
        <>
            <HeroImage />

            {showModal && (
                <div
                    className="modal"
                    style={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: 'rgba(0, 0, 0, 0.5)',
                        position: 'fixed',
                        top: 0,
                        left: 0,
                        right: 0,
                        bottom: 0,
                        zIndex: 9999,
                    }}
                >
                    <div
                        className="modal-dialog"
                        role="document"
                        style={{ width: '30%', textAlign: 'center' }}
                    >
                        <div className="modal-content">
                            <div
                                className="modal-header"
                                style={{
                                    backgroundColor: 'black',
                                    textAlign: 'center',
                                    position: 'relative',
                                    height: '60px',
                                }}
                            >
                                <button
                                    type="button"
                                    className="btn-close"
                                    onClick={closeModal}
                                    style={{
                                        position: 'absolute',
                                        top: '10px',
                                        right: '10px',
                                        color: '#fff',
                                        fontSize: '0.8rem',
                                    }}
                                    aria-label="Close"
                                ></button>
                                <div
                                    style={{
                                        display: 'flex',
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                    }}
                                >
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="35"
                                        height="35"
                                        fill="white"
                                        className="bi bi-exclamation-triangle"
                                        viewBox="0 0 16 16"
                                        style={{ marginRight: '5px' }}
                                    >
                                        <path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.15.15 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.2.2 0 0 1-.054.06.1.1 0 0 1-.066.017H1.146a.1.1 0 0 1-.066-.017.2.2 0 0 1-.054-.06.18.18 0 0 1 .002-.183L7.884 2.073a.15.15 0 0 1 .054-.057m1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767z" />
                                        <path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0M7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0z" />
                                    </svg>
                                    <h3 style={{ margin: '0', color: 'white' }}>
                                        Uh-oh!
                                    </h3>
                                </div>
                            </div>
                            <div
                                className="modal-body"
                                style={{
                                    paddingTop: '12px',
                                    paddingBottom: '0px',
                                    color: 'black'
                                }}
                            >
                                <p>{errorMessage}</p>
                            </div>
                            <div
                                className="modal-footer"
                                style={{
                                    display: 'flex',
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    backgroundColor: 'white',
                                    borderTop: 'none',
                                    paddingBottom: '10px',
                                    paddingTop: '0px',
                                }}
                            >
                                <button
                                    type="button"
                                    className="btn btn-secondary"
                                    onClick={closeModal}
                                    style={{ borderRadius: '0' }}
                                >
                                    Close
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            )}

            <div
                className="d-flex justify-content-center align-items-center"
                style={{
                    minHeight: '80vh',
                    backgroundColor: 'white',
                    paddingTop: '40px',
                }}
            >
                <div
                    className="container d-flex justify-content-between align-items-start"
                    style={{ paddingTop:'60px', paddingBottom: '100px' }}
                >
                    <div style={{ width: '30rem' }}>
                        <h2 className="text-center">Our Perks</h2>
                        <div
                            className="accordion"
                            id="accordionExample"
                            style={{ width: '100%' }}
                        >
                            {accordionItems.map((item, index) => (
                                <div className="accordion-item" key={index}>
                                    <h2
                                        className="accordion-header"
                                        id={`heading${index}`}
                                    >
                                        <button
                                            className={`accordion-button ${
                                                openAccordionItem === index
                                                    ? ''
                                                    : 'collapsed'
                                            }`}
                                            type="button"
                                            onClick={() =>
                                                toggleAccordionItem(index)
                                            }
                                            style={{ fontWeight: 'bold' }}
                                        >
                                            {item.title}
                                        </button>
                                    </h2>
                                    <div
                                        id={`collapse${index}`}
                                        className={`accordion-collapse collapse ${
                                            openAccordionItem === index
                                                ? 'show'
                                                : ''
                                        }`}
                                        aria-labelledby={`heading${index}`}
                                        data-bs-parent="#accordionExample"
                                    >
                                        <div className="accordion-body">
                                            {item.body}
                                        </div>
                                    </div>
                                </div>
                            ))}
                        </div>
                        <div
                            className="card text-white mb-3"
                            style={{
                                backgroundColor: '#F26925',
                                width: '100%',
                            }}
                        >
                            <iframe
                                width="100%"
                                height="315"
                                src="https://www.youtube.com/embed/5JNE5fpJk1Y?si=bgTaNmyItx95yDPb"
                                title="YouTube video player"
                                frameborder="0"
                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                                referrerpolicy="strict-origin-when-cross-origin"
                                allowfullscreen
                            ></iframe>
                            <div className="card-body">
                                <h5
                                    className="card-title text-center"
                                    style={{ color: '#0B0F1E' }}
                                >
                                    Life at Domuso
                                </h5>
                                <p className="card-text"></p>
                            </div>
                        </div>
                    </div>
                    <form
                        onSubmit={handleFormSubmit}
                        style={{
                            maxWidth: '30rem',
                            width: '100%',
                            height: '100%',
                            display: 'flex',
                            flexDirection: 'column',
                            justifyContent: 'space-between',
                        }}
                    >
                        <div
                            className="card text-white mb-6"
                            style={{
                                backgroundColor: '#0B0F1E',
                                flex: 'auto',
                            }}
                        >
                            <h3
                                className="card-header text-center"
                                style={{ color: '#F26925' }}
                            >
                                Join the Domuso Network
                            </h3>
                            <div className="card-body">
                                <div className="mb-">
                                    <label
                                        htmlFor="username"
                                        className="form-label"
                                    >
                                        Username
                                    </label>
                                    <input
                                        type="text"
                                        id="username"
                                        className="form-control"
                                        value={formData.username}
                                        onChange={(e) =>
                                            setFormData({
                                                ...formData,
                                                username: e.target.value,
                                            })
                                        }
                                        placeholder="Create a Username"
                                        required
                                    />
                                </div>
                                <div className="mb-3">
                                    <label
                                        htmlFor="password"
                                        className="form-label"
                                    >
                                        Password
                                    </label>
                                    <input
                                        type="password"
                                        id="password"
                                        className="form-control"
                                        value={formData.password}
                                        onChange={(e) =>
                                            setFormData({
                                                ...formData,
                                                password: e.target.value,
                                            })
                                        }
                                        placeholder="Enter Password"
                                        required
                                    />
                                </div>
                                <div className="mb-3">
                                    <label
                                        htmlFor="full_name"
                                        className="form-label"
                                    >
                                        Full Name
                                    </label>
                                    <input
                                        type="text"
                                        id="full_name"
                                        className="form-control"
                                        value={formData.full_name}
                                        onChange={(e) =>
                                            setFormData({
                                                ...formData,
                                                full_name: e.target.value,
                                            })
                                        }
                                        placeholder="Enter Full Name"
                                    />
                                </div>
                                <div className="mb-3">
                                    <label
                                        htmlFor="email"
                                        className="form-label"
                                    >
                                        Email
                                    </label>
                                    <input
                                        type="email"
                                        id="email"
                                        className="form-control"
                                        value={formData.email}
                                        onChange={(e) =>
                                            setFormData({
                                                ...formData,
                                                email: e.target.value,
                                            })
                                        }
                                        placeholder="Enter Email"
                                        required
                                    />
                                </div>
                                <div className="mb-3">
                                    <label
                                        htmlFor="linkedin_url"
                                        className="form-label"
                                    >
                                        LinkedIn URL
                                    </label>
                                    <input
                                        type="text"
                                        id="linkedin_url"
                                        className="form-control"
                                        value={formData.linkedin_url}
                                        onChange={(e) =>
                                            setFormData({
                                                ...formData,
                                                linkedin_url: e.target.value,
                                            })
                                        }
                                        placeholder="Enter LinkedIn URL"
                                        required
                                    />
                                </div>
                            </div>
                            <div
                                className="card-footer"
                                style={{
                                    backgroundColor: '#0B0F1E',
                                    paddingBottom: '20px',
                                }}
                            >
                                <div
                                    style={{
                                        display: 'flex',
                                        justifyContent: 'center',
                                    }}
                                >
                                    <button
                                        type="submit"
                                        className="btn"
                                        style={{
                                            backgroundColor: '#F26925',
                                            color: 'white',
                                            width: '40%',
                                        }}
                                        onClick={handleFormSubmit}
                                        required
                                    >
                                        Sign Up
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </>
    )
}
